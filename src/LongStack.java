import java.util.LinkedList;
import java.lang.RuntimeException;
import java.util.StringTokenizer;

public class LongStack {

   private LinkedList<Long> ll;

   public static void main (String[] argum) {
      long temp = interpret ("-234 45");
      System.out.println(""+temp);
   }

   LongStack() {
      ll = new LinkedList<Long>();
   }

   @Override
   public Object clone() throws CloneNotSupportedException {

      LongStack copy = new LongStack();
      copy.ll = (LinkedList<Long>)this.ll.clone();
      return copy;
   }

   public boolean stEmpty() {
      return ll.isEmpty();
   }

   public void push (long a) {
      ll.addLast(a);
   }

   public long pop() {
      if(ll.isEmpty()){
         throw new RuntimeException("Error, Long stack is empty");
      }
      return ll.pollLast();
   }

   public void op (String s) {
      if(ll.size() <= 1 ){
         throw new java.lang.RuntimeException("Error, less than two elements.");
      }
      if(s.length() != 1){
         throw new java.lang.RuntimeException("Error, illegal size in s.");
      }

      long firstOut = ll.getLast();
      ll.removeLast();

      long secondOut = ll.getLast();
      ll.removeLast();


      if(s.equals("+")){
         ll.addLast(firstOut+secondOut);
      }
      else if(s.equals("-")){
         ll.addLast(secondOut - firstOut);
      }
      else if(s.equals("*")){
         ll.addLast(firstOut*secondOut);
      }
      else if(s.equals("/")){
         ll.addLast(secondOut/firstOut);
      }
      else{
         throw new RuntimeException("Error,unknown operator in string s.");
      }

   }

   public long tos() {
      if(ll.isEmpty()){
         throw new RuntimeException("Error, Long stack is empty");
      }
      return ll.getLast();
   }

   @Override
   public boolean equals (Object o) {
      LongStack comp = (LongStack)o;
      if(comp.ll.equals(this.ll)){
         return true;
      }
      return false;
   }

   @Override
   public String toString() {

      return ll.toString();
   }


   public static long interpret (String pol) {

      if(pol.trim().equals("")){
         throw new RuntimeException("Error, empty expression");
      }

      StringTokenizer st = new StringTokenizer(pol);

      LongStack stack = new LongStack();

      while(st.hasMoreTokens()){

         String s = st.nextToken();

         if(s.equals("+") || s.equals("-") || s.equals("*") || s.equals("/")){

            try {
               if (stack.ll.size() != 0) {
                  stack.op(s);

               }

               else{
                  throw new RuntimeException("Error, illegal string pol ( " + pol + " ) stack underflow");
               }
            }
            catch(RuntimeException e){
               throw new RuntimeException(""+e+" user input: ( " + pol + " )");
            }
         }

         else {

            try{
               stack.push(Long.parseLong(s));
            }

            catch (NumberFormatException e){

               throw new RuntimeException("Error, illegal string pol ( " + pol + " ) unknown operator");
            }

         }
      }

      if(stack.ll.size() != 1){
         throw new RuntimeException("Error, illegal string pol ( " + pol + " ) redundant element left in LongStack");
      }

      return stack.pop();
   }

}